import java.util.ArrayList;
import java.util.Scanner;

public class Ladung {
    /*
       ###################
       #### Attribute ####
       ###################
    */
    private String name;
    private int anzahl;
    private String grund;

    /*
       ###################
       ### Konstruktor ###
       ###################
    */
    Ladung(String name, int anzahl, Inventar gruende) {
        this.name = name;
        this.anzahl = anzahl;
        this.grund = gruende.toString();
    }

    /*
       ###################
       # Getter & Setter #
       ###################
    */
    public int getAnzahl() {
        return anzahl;
    }
    public String getName() {
        return name;
    }
    public String getGrund() {
        return grund;
    }

    public void setName(String name) {
        this.name = name;
    }
    public void setAnzahl(int anzahl) {
        this.anzahl = anzahl;
    }
    public void setGrund(Inventar grund) {
        this.grund = grund.toString();
    }

    /*
       ###################
       ###  Funktionen ###
       ###################
    */

    /**
     * <h1>Ladungsvorlagen fuer verschiedene Szenarien</h1>
     * Falls ein Schiffs-Array mit nicht-standartisiertem Namen uebergeben wird, so wird erst ermittelt ob der Feind die
     * Enterprise ist, oder ob der Feind die Borg ist. Dies wird benoetigt, weil man selbst entscheiden kann wenn man
     * ein Charakter erstellt, ob man ein Borg, oder ein Mensch ist.
     * <br>
     * Im Falle dass die Standard Charaktaere geladen werden, wird dies erkannt und die Ladungen werden entsprechend
     * gesetzt<br><br>
     * @author Daniel Garkavy
     * @param raumschiff Schiffs-Array
     * @param in Scanner
     * @since 2021-04-24
     */

    protected static void ladungsVorlagen(Raumschiff[] raumschiff, Scanner in) {

        // Vorlagen für verschiedene Ladungsszenarien
        // --------Du bist die Borg--------
        if (raumschiff[0].getName().equals("Borg")) {

            ArrayList<Ladung> ladungen = new ArrayList<>();
            ladungen.add(new Ladung("Klingonen", 200, Inventar.ASSIMILIERUNG));
            ladungen.add(new Ladung("Romulaner", 300, Inventar.ASSIMILIERUNG));
            ladungen.add(new Ladung("Vulkanier", 150, Inventar.ASSIMILIERUNG));

            Raumschiff.charArPrint("\nLadung der Borg:", 20);
            Raumschiff.charArPrint("\nEs sind 200 Klingonen, 300 Romulaner und 150 Vulkanier an Bord\n\n", 20);

            raumschiff[0].setLadung(ladungen);
            raumschiff[0].getLadung().add(new Ladung("Photonentorpedos", raumschiff[0].getPhotonenTorpedo(), Inventar.WAFFEN));



            ladungen = new ArrayList<>();
            ladungen.add(new Ladung("Tricoder", 120, Inventar.FORSCHUNG));
            ladungen.add(new Ladung("Forschungssonden", 200, Inventar.FORSCHUNG));

            Raumschiff.charArPrint("\nLadung der Enterprise: ", 20);
            Raumschiff.charArPrint("\nDie Enterprise hat 120 Tricoder und 200 Forschungssonden an Bord\n\n", 20);

            raumschiff[1].setLadung(ladungen);
            raumschiff[1].getLadung().add(new Ladung("Photonentorpedos", raumschiff[1].getPhotonenTorpedo(), Inventar.WAFFEN));
        }

        // ---------Kreierter Charakter ODER Gegner Enterprise---------

        else if (!raumschiff[0].getName().equals("Borg")) {
            ArrayList<Ladung> ladungen = raumschiff[0].addLadung(in);
            raumschiff[0].setLadung(ladungen);
            raumschiff[0].getLadung().add(new Ladung("Photonentorpedos", raumschiff[0].getPhotonenTorpedo(), Inventar.WAFFEN));

            if (raumschiff[1].getName().equals("Enterprise")) {
                ladungen = new ArrayList<>();
                ladungen.add(new Ladung("Tricoder", 120, Inventar.FORSCHUNG));
                ladungen.add(new Ladung("Forschungssonden", 200, Inventar.FORSCHUNG));

                Raumschiff.charArPrint("\nLadung der Enterprise: ", 20);
                Raumschiff.charArPrint("\nDie Enterprise hat 120 Tricoder und 200 Forschungssonden an Bord\n\n", 20);
            }

            else {
                ladungen = new ArrayList<>();
                ladungen.add(new Ladung("Klingonen", 200, Inventar.ASSIMILIERUNG));
                ladungen.add(new Ladung("Romulaner", 300, Inventar.ASSIMILIERUNG));
                ladungen.add(new Ladung("Vulkanier", 150, Inventar.ASSIMILIERUNG));

                Raumschiff.charArPrint("\nLadung der Borg:", 20);
                Raumschiff.charArPrint("\nEs sind 200 Klingonen, 300 Romulaner und 150 Vulkanier an Bord\n\n", 20);
            }

            raumschiff[1].setLadung(ladungen);
            raumschiff[1].getLadung().add(new Ladung("Photonentorpedos", raumschiff[1].getPhotonenTorpedo(), Inventar.WAFFEN));
        }
    }
}