import java.io.File;
import java.util.Scanner;

/*
     ~ ############################## ~
   ~ ~ ### Main Method Enterprise ### ~ ~
     ~ ############################## ~
*/

public class Main {

    public static void main(String[] args) {

        Scanner in = new Scanner(System.in);
        File saveFile = new File("SpeicherFile.txt");
        Raumschiff[] raumschiff;

        if (saveFile.exists() && saveFile.length() != 0) {
            Raumschiff.charArPrint("Speicherdatei gefunden", 10);
            Raumschiff.waitPrint('.', 50, 5);
            Raumschiff.waitPrint('\n', 100, 1);

            raumschiff = Captain.ladeFile();

            Raumschiff.charArPrint("Wird gestartet!\n\n\n\n", 70);
            Raumschiff.charArPrint("(Falls sie nicht die Speicherdatei beim start laden wollen, nutzen sie \"beenden loeschen\")\n", 40);
        }

        else {
            Captain[] kapitaen = Raumschiff.addKapitaen(in);
            raumschiff = Raumschiff.addSchiff(in, kapitaen);

            Ladung.ladungsVorlagen(raumschiff, in);
            System.out.println("\nBereit für Befehle Captain!");
            in.nextLine();
        }


        // Konsole fuer Befehle wie "abschiessen" oder "reparieren"
        int schiff = 0;
        int feind = 1;
        boolean konsole = true;

        while (konsole) {

            System.out.print("Befehl (\"help\" fuer Hilfe): ");
            String[] befehl = in.nextLine().split(" ");

            if (befehl.length >= 2) {
                switch (befehl[0]) {

                    case "abschiessen" : if (befehl[1].equalsIgnoreCase("torpedo")) {raumschiff[schiff].torpedoSchuss(raumschiff[feind], in);}
                    else if (befehl[1].equalsIgnoreCase("phaser")) {raumschiff[schiff].phaserSchuss(raumschiff[feind]);}
                    else {System.out.println("Parameter nicht erkannt");}
                        break;

                    case "logbuch"     : if (befehl[1].equalsIgnoreCase("lesen")) {raumschiff[schiff].logbuchLesen(raumschiff[schiff].getLogBuch());}
                    else if (befehl[1].equalsIgnoreCase("loeschen")) {raumschiff[schiff].logbuchLoeschen(raumschiff[schiff].getLogBuch(), in); }
                    else {System.out.println("Parameter nicht erkannt");}
                        break;

                    case "broadcast"   : if (befehl[1].equalsIgnoreCase("senden")) {raumschiff[feind].setBroadcastCom(raumschiff[schiff].brdCaster(in, raumschiff[feind], true));}
                    else if (befehl[1].equalsIgnoreCase("empfangen")) {raumschiff[schiff].setBroadcastCom(raumschiff[feind].brdCaster(null, raumschiff[schiff], false));}
                    else if (befehl[1].equalsIgnoreCase("lesen")) {Raumschiff.charArPrint(raumschiff[schiff].getBroadcastCom()+"\n", 1);}
                    else {System.out.println("Parameter nicht erkannt");}
                        break;

                    case "tauschen"    : if (befehl[1].equalsIgnoreCase("feind")) {schiff = 1; feind = 0; Raumschiff.charArPrint("Du bist der Feind.\n", 15);}
                    else if (befehl[1].equalsIgnoreCase("zurueck")) {schiff = 0; feind = 1; Raumschiff.charArPrint("Du bist wieder der Captain!\n", 15);}
                    else {System.out.println("Parameter nicht erkannt");}
                        break;

                    case "reparieren"  : if (befehl[1].equalsIgnoreCase("huelle")) {raumschiff[schiff].setHuelle(raumschiff[schiff].getHuelle() + raumschiff[schiff].reparieren(in));}
                    else if (befehl[1].equalsIgnoreCase("schild")) {raumschiff[schiff].setHuelle(raumschiff[schiff].getSchild() + raumschiff[schiff].reparieren(in));}
                    else if (befehl[1].equalsIgnoreCase("energie")) {raumschiff[schiff].setHuelle(raumschiff[schiff].getEnergie() + raumschiff[schiff].reparieren(in));}
                    else {System.out.println("Parameter nicht erkannt");}
                        break;

                    case "beenden"     : if (befehl[1].equalsIgnoreCase("speichern")) {konsole = Captain.speicherFile(raumschiff);}
                    else if (befehl[1].equalsIgnoreCase("loeschen")) {konsole = Captain.loescheSaveFile();}
                    else if (befehl[1].equalsIgnoreCase("schliessen")) {konsole = false;}
                    else {System.out.println("Parameter nicht erkannt");}
                        break;

                    default: System.out.println("Befehl nicht erkannt");
                }
            }

            else if (befehl.length == 1) {
                switch (befehl[0]) {

                    case "help" -> raumschiff[schiff].hilfe();
                    case "status" -> raumschiff[schiff].schiffStatus();
                    case "ladung" -> raumschiff[schiff].ladungsVerzeichnis(raumschiff[schiff].getLadung());
                    default -> System.out.println("Befehl nicht erkannt");
                }
            }
        }
        System.exit(0);
    }
}
