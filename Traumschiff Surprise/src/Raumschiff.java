import java.security.SecureRandom; // <-- Benutzt Entropie vom Betriebssystem/Besser als nur Random
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Scanner;

public class Raumschiff {

    /*
       ###################
       #### Attribute ####
       ###################
    */

    private String name;
    private String broadcastCom;
    private ArrayList<String> logBuch;

    private double energie;
    private double schild;
    private double huelle; // <-- Die Außenhülle spiegelt das Leben des Schiffes wieder

    private int photonenTorpedo;
    private int android;

    private Captain kapitaen;
    private ArrayList<Ladung> ladung;

    /*
       ###################
       ### Konstruktor ###
       ###################
    */

    Raumschiff() {}
    Raumschiff(String name, String broadcastCom, double energie,
               double schild, double huelle, int photonenTorpedo, int android, Captain kapitaen) {
        this.name = name;
        this.broadcastCom = broadcastCom;
        this.logBuch = new ArrayList<>();

        this.energie = energie;
        this.schild = schild;
        this.huelle = huelle;

        this.photonenTorpedo = photonenTorpedo;
        this.android = android;

        this.kapitaen = kapitaen;
    }

    /*
       ###################
       # Getter & Setter #
       ###################
    */

    String getName() {
        return this.name;
    }
    void setName(String name) {
        this.name = name;
    }

    String getBroadcastCom() {
        return this.broadcastCom;
    }
    void setBroadcastCom(String brdcst) {
        this.broadcastCom = brdcst;
    }

    ArrayList<String> getLogBuch() {
        return this.logBuch;
    }
    void setLogBuch(ArrayList<String> arrayList) {
        this.logBuch = arrayList;
    }

    double getEnergie() {
        return this.energie;
    }
    void setEnergie(double energie) {
        this.energie = energie;
    }

    double getSchild() {
        return this.schild;
    }
    void setSchild(double schild) {
        this.schild = schild;
    }

    double getHuelle() {
        return this.huelle;
    }
    void setHuelle(double huelle) {
        this.huelle = huelle;
    }

    int getPhotonenTorpedo() {
        return this.photonenTorpedo;
    }
    void setPhotonenTorpedo(int photonenTorpedo) {
        this.photonenTorpedo = photonenTorpedo;
    }

    int getAndroid() {
        return this.android;
    }
    void setAndroid(int android) {
        this.android = android;
    }

    Captain getKapitaen() {
        return this.kapitaen;
    }
    void setKapitaen(Captain kapitaen) {
        this.kapitaen = kapitaen;
    }

    ArrayList<Ladung> getLadung() {
        return this.ladung;
    }
    void setLadung(ArrayList<Ladung> ladungen) {
        this.ladung = ladungen;
    }

    /*
       ###################
       ###  Funktionen ###
       ###################
    */

    /**
     * <h1>Print & Input Processing</h1>
     * <span style="color:orange">charArPrint()</span>: Nimmt einen String, spaltet ihn in ein Char Array und nutzt
     * <i style="color:orange">waitPrint()</i> in einer for Loop (gibt individuellen Char ein)<br><br>
     * <span style="color:orange">waitPrint()</span>: Nimmt ein Char c, Millisekunden in int und wie oft es looped in int
     * und printed es. Ruft danach Thread.sleep auf um zu warten<br><br>
     * <span style="color:orange">enumListePrint()</span>: Nimmt Wahl (choice) in int um ein Teil der Enumerator Liste
     * zu printen<br><br>
     * <span style="color:orange">yesno()</span>: Nimmt einen Scanner und gibt einen boolean zurueck. Es gibt
     * <b style="color:green">true</b> zurueck wenn man <i>"y" (yes)</i> eingibt und <b style="color:red">false</b> wenn etwas anderes.
     * @author Daniel Garkavy
     * @param satz String
     * @param millis int
     */

    protected static void charArPrint(String satz, int millis) {
        char[] sArray = satz.toCharArray();
        for (char c : sArray) {
            waitPrint(c, millis, 1);
        }
    }
    protected static void waitPrint(char c, int milli, int loopFor) {
        for (int i = 0; i < loopFor; i++) {
            System.out.print(c);
            try {
                Thread.sleep(milli);
            } catch (InterruptedException ex) {
                ex.printStackTrace();
            }
        }
    }
    protected static void enumListePrint(int choice) {

        // 0 = Printed alles
        // 1 = Printed Geschlecht
        // 2 = Printed Grund

        String[] ar = new String[Inventar.values().length];
        // Geschlecht
        ar[0] = Inventar.MAENNLICH.toString();
        ar[1] = Inventar.WEIBLICH.toString();
        // Grund
        ar[2] = Inventar.FORSCHUNG.toString();
        ar[3] = Inventar.ASSIMILIERUNG.toString();


        // Printe Geschlecht
        if (choice == 1) {
            for (int i = 0; i < ar.length - 6; i++) {
                System.out.printf("[%d] %s\n", (i + 1), ar[i]);
            }
            System.out.println("\n");
        }

        // Printe Grund
        else if (choice == 2){
            for (int i = 2; i < ar.length - 4; i++) {
                System.out.printf("[%d] %s\n", (i - 1), ar[i]);
            }
            System.out.println("\n");
        }

        // Printe beides
        else {
            System.out.println("Geschlecht:");
            for (int i = 0; i < ar.length - 2; i++) {
                System.out.printf("[%d] %s\n", i, ar[i]);
            }
            System.out.println("\n");
            System.out.println("Grund:");
            for (int i = 2; i < ar.length; i++) {
                System.out.printf("[%d] %s\n", i, ar[i]);
            }
            System.out.println("\n");
        }
    }
    protected static boolean yesno(Scanner in) {
        String var = in.next();
        return var.equalsIgnoreCase("Y");
    }

    /**
     * <h1>Adder Methoden</h1>
     * <span style="color:orange">addKapitaen()</span>: Nimmt einen Scanner und ermittelt, ob die Standard
     * Charaktaere geladen werden sollen, kreiert ansonsten einen eigenen Charakter. Der Charakter wird dann
     * in einem Captain Array zurueckgegeben.<br><br>
     * <span style="color:orange">addSchiff()</span>: Nimmt die vorherig erstellten Kapitaene und urteilt danach,
     * ob die Standard Schiffe erstellt werden. Wenn nicht, kann man selbst ein Schiff kreieren und der Feind wird dann
     * nach Auswahl der Rasse erzeugt (wenn Mensch, dann ist die Borg der Feind und umgekehrt). <br><br>
     * <span style="color:orange">addLadung()</span>: Wird aufgerufen wenn ein nicht-standard Charakter erstellt worden
     * ist um eigene Ladungen hinzufuegen zu koennen. Wird von <i style="color:orange">Ladung.ladungsVorlagen()</i>
     * aufgerufen wenn benötigt und gibt das erstellte Ladungsarray zurueck.
     * @param in Scanner
     * @return Captain[]
     */

    static Captain[] addKapitaen(Scanner in) {
        Captain[] kap = new Captain[2];
        System.out.print("Sollen die Standard Kapitaene erzeugt werden(Borg/Mensch)? (y/n): ");

        if (yesno(in)) {
            kap[0] = new Captain("Borg-Koenigin", true, Inventar.WEIBLICH, 1774);
            kap[1] = new Captain("Jean-Luc Picard", false, Inventar.MAENNLICH, 2364);
            return kap;
        }

        charArPrint("Du bist der Kapitaen.", 40);
        waitPrint('\n',400,1);
        charArPrint("Waehle deinen Namen: ", 70);
        in.nextLine(); // <-- Löscht den Buffer
        String name = in.nextLine();
        System.out.println();
        System.out.println("Bist du ein Borg, oder ein Mensch?: ");

        int auswahl = 0;
        boolean race = true;
        while (auswahl == 0) {

            System.out.println("[1] Borg\n[2] Mensch");
            System.out.print("\n\nAuswahl: ");

            switch (in.nextInt()) {
                case 1 -> auswahl++;
                case 2 -> {
                    race = false;
                    auswahl++;
                }
                default -> System.out.println("Bitte gueltige Auswahl nehmen.");
            }
        }

        System.out.print("Besitzt du mehr als ein Schiff (y/n)?: ");
        boolean schiffe = yesno(in);

        System.out.println("\n");
        System.out.print("Bist du Maennlich oder Weiblich?: \n");

        Inventar geschlecht = null;
        auswahl = 0;

        while (auswahl == 0) {
            enumListePrint(1);
            System.out.print("\n\nAuswahl: ");

            switch (in.nextInt()) {
                case 1 -> {
                    geschlecht = Inventar.MAENNLICH;
                    auswahl++;
                }
                case 2 -> {
                    geschlecht = Inventar.WEIBLICH;
                    auswahl++;
                }
                default -> System.out.println("Bitte gueltige Auswahl nehmen.");
            }
        }

        System.out.print("Seit wann bist du Kapitaen?: ");
        int capSeit = in.nextInt();
        char[] chars = ("" + capSeit).toCharArray();

        while (chars.length >= 5 || chars.length < 1) {
            System.out.println("Erlaubtes Datum: 1 - 9999\nBitte ernuet versuchen: ");
            capSeit = in.nextInt();
            chars = ("" + capSeit).toCharArray();
        }

        charArPrint("Du heisst "+name+", bist "+geschlecht.toString().toLowerCase()+" und Kapitaen seit "+capSeit+"\n", 50);
        kap[0] = new Captain(name, schiffe, geschlecht, capSeit);

        if (race) {
            charArPrint("Dein Feind ist Jean-Luc Picard, ein Forscher. Er ist maennlich und Kapitaen seit 2364"+"\n", 60);
            kap[1] = new Captain("Jean-Luc Picard", false, Inventar.MAENNLICH, 2364);
        }
        else {
            charArPrint("Dein Feind ist die Borg-Koenigin der Borg. Sie ist weiblich und Kapitaen seit 1774"+"\n",60);
            kap[1] = new Captain("Borg-Koenigin", true, Inventar.WEIBLICH, 1774);
        }

        System.out.println("Kapitaene erstellt.\n\n\n\n");
        return kap;
    }
    static Raumschiff[] addSchiff(Scanner in, Captain[] kapitaen) {
        Raumschiff[] rschiffe = new Raumschiff[2];
        if (kapitaen[0].getName().equals("Borg-Koenigin") & kapitaen[1].getName().equals("Jean-Luc Picard"))
        {
            System.out.println("Standard Schiffe erstellt!");
            waitPrint('\n', 2000, 1);
            charArPrint("Du bist die Borg", 20);
            waitPrint('.', 100, 3);
            charArPrint(" du Assimilierst gerne Voelker!", 30);
            waitPrint(' ', 1000, 1);
            System.out.println();
            charArPrint("Du hast 120% Energie, 100% Schild und 100% Huelle (Leben)", 10);
            waitPrint('.', 200, 5);
            charArPrint(" Photonentorpedos hast du momentan keine", 50);
            waitPrint(' ', 900, 3);

            rschiffe[0] = new Raumschiff("Borg","Ich bin die Borg... halloechen :)", 1.2, 1.0, 1.0, 0, 3, kapitaen[0]);
            rschiffe[1] = new Raumschiff("Enterprise", "Ich bin die Enterprise... heyo!", 1.0, 1.0, 1.0, 1, 3, kapitaen[1]);
            return rschiffe;
        }

        boolean forscherFeind = (kapitaen[1].getName().equals("Jean-Luc Picard"));
        boolean borgFeind = (kapitaen[1].getName().equals("Borg-Koenigin"));

        charArPrint("Du begibst dich zu deinem Raumschiff.... wie heisst es nochmal?\nName: ", 30);

        in.nextLine();
        String name = in.nextLine();

        System.out.print("Ahh, genau, du begibst dich zu " + name.toUpperCase() + "\nDein Schiff besitz ein Broadcaster, " +
                "willst du eine Standard Nachricht einstellen? (y/n): ");

        boolean janein = yesno(in);
        System.out.println();

        String nachricht = "";
        if (janein) {
            System.out.print("Gib dein Standard Broadcast ein: ");
            in.nextLine();
            nachricht = in.nextLine();
        }

        else {
            System.out.println("Keine Standard Nachricht gewaehlt (kann spaeter geaendert werden)");
        }

        System.out.print("Du bemerkst, dass dein Schiff ein Schild hat.\nWas für eine Kapazitaet hat es? (z.B 0,9 = 90%): ");
        System.out.print("\n\nAuswahl: ");
        double schild = in.nextDouble();

        System.out.print("Es besitzt zusaetzlich noch Energie.\nWelche Energiekapazitaet besitzt das Schiff? (auch prozentuell): ");
        System.out.print("\n\nAuswahl: ");
        double energie = in.nextDouble();

        System.out.print("Alle guten Schiffe haben noch eine Huelle. :)\nWie ist hier die Kapazitaet?: ");
        System.out.print("\n\nAuswahl: ");
        double huelle = in.nextDouble();

        System.out.print("\n\n\n\n");

        System.out.print("... Ich habe vergessen, dass du ein Photonentorpedo-System besitzt.\nHat es überhaupt Torpedos? (y/n): ");
        janein = yesno(in);
        int torpedo = 0;

        if (janein) {
            System.out.print("Aha! Wieviele denn?: ");
            torpedo = in.nextInt();
        }

        else {
            System.out.println("Schade, kann man aber nachkaufen");
        }

        charArPrint("Jedes Schiff besitzt jeweils 3 Reparatur-Androiden", 30);
        waitPrint('\n', 400, 3);
        waitPrint('\n', 900, 1);

        rschiffe[0] = new Raumschiff(name, nachricht, energie, schild, huelle, torpedo, 3, kapitaen[0]);
        if (borgFeind) {
            rschiffe[1] = new Raumschiff("Borg","Ich bin die Borg... halloechen :)", 1.2, 1.0, 1.0, 0, 3, kapitaen[0]);
        }

        else if (forscherFeind) {
            rschiffe[1] = new Raumschiff("Enterprise", "Ich bin die Enterprise... heyo!", 1.0, 1.0, 1.0, 1, 3, kapitaen[1]);
        }

        return rschiffe;
    }
    ArrayList<Ladung> addLadung(Scanner in) {

        System.out.print("Das Schiff besitzt genug Platz für viele Ladungen");
        waitPrint('\n', 400, 1);
        charArPrint("Wieviele Arten von Ladungen soll das Schiff haben?: ", 30);

        int anzahlLadung = in.nextInt();
        ArrayList<Ladung> ladungen = new ArrayList<>();

        for (int i = 0; i < anzahlLadung; i++) {

            in.nextLine();
            System.out.print("Wie soll Ladung " + (i + 1) + " heissen?: ");
            String name = in.nextLine();

            System.out.print("Wie viel(e) " + name + " soll die Ladung " + (i + 1) + " haben?: ");
            int anzahl = in.nextInt();

            System.out.println("Was fuer ein Zweck hat die Ladung?: ");
            Inventar gruende = null;

            int count = 0;
            while (count == 0) {

                enumListePrint(2);
                System.out.print("Ihre Wahl: ");

                switch (in.nextInt()) {

                    case 1 -> {
                        gruende = Inventar.FORSCHUNG;
                        count++;
                    }

                    case 2 -> {
                        gruende = Inventar.ASSIMILIERUNG;
                        count++;
                    }

                    default -> System.out.println("Bitte gueltige Option nehmen");
                }
            }

            ladungen.add(new Ladung(name, anzahl, gruende));
            charArPrint("\nLadung " + (i + 1) + " heisst " + ladungen.get(i).getName() + ", Stueckzahl " + ladungen.get(i).getAnzahl()
                    + " und dient der " + ladungen.get(i).getGrund() + "\n", 60);

        }

        return ladungen;
    }

    /**
     * <h1>Hilfsausgaben (Status & Hilfe)</h1>
     * <span style="color:orange">schiffStatus()</span>: Printed alle Attribute mit getter methoden vom Schiffsobjekt,
     * das die Methode aufgerufen hat. Float Werte werden gerundet via String.format, da durch <b>SecureRandom</b>
     * ein langer float Wert entsteht (mit großer Entropie).<br><br>
     * <span style="color:orange">hilfe()</span>: Hat einen vordefinierten String Array mit allen Befehlen und parametern
     * die ausfuehrbar/definierbar sind und gibt diese in einer <i style="color:green">foreach</i> loop aus.
     * <br><br>
     * @author Daniel Garkavy
     * @since 21-04-24
     */

    void schiffStatus() {
        String[] status = new String[]{"Kapitaen: "+kapitaen.getName(), "Schiffsname: "+getName(),
                "Broadcast Nachricht: "+getBroadcastCom(), "Logbuch Eintraege: " + getLogBuch().size(),
                String.format("Energieversorgung: %.2f%%",(getEnergie()*100)),
                String.format("Schild: %.2f%%",(getSchild()*100)),
                String.format("Huelle: %.2f%%",(getHuelle()*100)),
                "Androiden: "+getAndroid(), "Torpedos: "+getPhotonenTorpedo()
        };

        for (String s : status) {
            System.out.println(s);
        }
    }
    void hilfe() {
        String[] alleBefehle = new String[]{"\nhelp - Zeigt alle Befehle die zur Verfuegung stehen an", "status - Zeigt" +
                " den Status des Schiffes an", "ladung - Zeigt das Ladungsverzeichnis an", "abschiessen - " +
                "Schiesst Waffe deiner Wahl ab (beispiel: \"abschiessen torpedo\")\n\tArgumente:\n\t\ttorpedo - Schiesst eine bestimmte Menge an" +
                " Photonentorpedos ab\n\t\tphaser - Schiesst Phaserkanone ab\n", "logbuch - " +
                "Logbuch auslesen oder bearbeiten (beispiel: \"logbuch lesen\")\n\tArgumente:\n\t\tlesen - Gibt Logbuch aus\n\t\tloeschen - Loescht " +
                "alle Logbuch Eintraege\n","broadcast - Nachrichten senden oder empfangen (\"broadcast senden\")\n\tArgumente:\n\t\t" +
                "senden - Sendet dem Feind eine Nachricht\n\t\tempfangen - Empfaengt Nachricht vom Feindlichem Broadcaster " +
                "\n\t\tlesen - Sieht die Nachricht die sich im Broadcaster befindet\n", "tauschen - Vertauscht die Rollen" +
                " von dir und dem Feind (beispiel: \"tauschen feind\")\n\tArgumente:\n\t\tfeind - Du bist der Feind\n\t\t" +
                "zurueck - Wechselt zum originalem Kapitaen zurueck\n", "reparieren - Repariert ein Teil des Schiffes" +
                " (beispiel: \"reparieren huelle\")\n\tArgumente:\n\t\thuelle - Repariert die Huelle des Schiffes\n\t\t" +
                "schild - Repariert den Schild\n\t\tenergie - Repariert das Energiesystem\n", "beenden - Speichert und beendet das Spiel" +
                " (beispiel: \"beenden speichern\")\n\tArgumente:\n\t\tspeichern - Speichert und beendet Prozess" +
                "\n\t\tloeschen - Loescht save und beendet Prozess\n\t\tschliessen - Schliesst das Spiel ohne zu speicher\n"
        };

        for (String s : alleBefehle) {
            System.out.println(s);
        }
    }

    /**
     * <h1>Treffer Methoden</h1>
     * <span style="color:orange">torpedoSchuss()</span>: Nimmt ein feindliches Schiff und einen Scanner, ermittelt
     * wieviele Torpedos eingesetzt werden koennen und fuegt Logbucheintraege hinzu. Reduziert die Torpedoanzahl
     * von Objekt, dass es aufherufen hat in einer for loop und uebergibt das feindliche Objekt an
     * <i style="color:orange">treffer()</i> um den Schaden zu errechnen.<br><br>
     * <span style="color:orange">phaserSchuss()</span>: Nimmt ein feindliches Schiffsobjekt, zieht 50% Energie vom
     * Schiffsobjekt ab, dass es aufgerufen hat (wenn genug energie vorhanden),
     * fuegt einen Logbucheintrag hinzu und uebergibt das feindliche Objekt an <i style="color:orange">treffer()</i>.
     * <br><br>
     * @author Daniel Garkavy
     * @since 25-04-2021
     * @param feindSchiff Raumschiff
     * @param in Scanner
     */

    void torpedoSchuss(Raumschiff feindSchiff, Scanner in) {
        Date uhr = new Date();
        SimpleDateFormat uhrF = new SimpleDateFormat("HH:mm:ss");

        if (getPhotonenTorpedo() <= 0) {
            feindSchiff.setBroadcastCom("-=*Click*=-");
            logBuch.add(uhrF.format(uhr)+" Torpedosystem: -=*Click*=-");
            feindSchiff.logBuch.add(uhrF.format(uhr)+" "+getName()+": -=*Click*=-");
            System.out.println("Keine Photonentorpedos gefunden!");
        }

        else {
            charArPrint("Wieviele Torpedos feuern?: ", 18);

            int torpedosZumFeuern = in.nextInt();
            if (torpedosZumFeuern > getPhotonenTorpedo()) {

                torpedosZumFeuern = getPhotonenTorpedo();
                charArPrint("Alle Torpedos werden gefeuert.\n", 10);
            }

            System.out.printf("[%d] Torpedo(s) eingesetzt", torpedosZumFeuern);
            waitPrint('\n',300, 1);

            for (int i = torpedosZumFeuern; i > 0; i--) {

                setPhotonenTorpedo(getPhotonenTorpedo() - 1);
                logBuch.add(uhrF.format(uhr) + " Du hast auf " + feindSchiff.getName() + " gefeuert mit [Torpedos]");
                feindSchiff.logBuch.add(uhrF.format(uhr) + " " + getName() + " hat auf sie gefeuert mit Torpedos gefeuert!");
                System.out.println("Torpedo abgefeuert! Es verbleiben " + getPhotonenTorpedo() + " Torpedos.");
                treffer(feindSchiff);
            }
            setLadung(ladungEntfernen(getLadung()));

            System.out.println();
            in.nextLine();
        }
    }
    void phaserSchuss(Raumschiff feindSchiff) {
        Date uhr = new Date();
        SimpleDateFormat uhrF = new SimpleDateFormat("HH:mm:ss");

        if (getEnergie() < 0.5) {
            System.out.println("Nicht genug Energie!");
            feindSchiff.setBroadcastCom("-=*Click*=-");
            feindSchiff.logBuch.add(uhrF.format(uhr)+" "+getName()+": -=*Click*=-");
            logBuch.add(uhrF.format(uhr)+" Phaserkanonen System: -=*Click*=-");
        }

        else {
            setEnergie(getEnergie() - 0.5);
            logBuch.add(uhrF.format(uhr)+" Du hast auf " + feindSchiff.getName() + " gefeuert mit [Phaserkanonen]");
            feindSchiff.logBuch.add(uhrF.format(uhr)+" "+getName() + " hat auf sie mit Phaserkanonen gefeuert!");
            System.out.println("Phaser abgefeuert! Es verbleibt " + (getEnergie() * 100) + "% Energie.");
            treffer(feindSchiff);
        }
    }

    /**
     * <h1>Inventar, Nachrichten & Logbuch</h1>
     * <span style="color:orange">ladungsVerzeichnis()</span>: Nimmt die Ladung per Getter vom aufrufendem Objekt und
     * printed mit printf die Anzahl, den Namen und den Grund vom Objekt.<br><br>
     * <span style="color:orange">ladungsEntfernen()</span>: Nimmt die Ladung die vom Objekt gegetted wird,
     * loescht die Elemente mit Anzahl == 0 in einer for loop und gibt die modifizierte ArrayList zurueck.<br><br>
     * <span style="color:orange">logbuchLesen()</span>: Nimmt die Logbuch-ArrayList per Getter vom aufrufendem Objekt und
     * printed die alle Eintraege in einer for loop.<br><br>
     * <span style="color:orange">logbuchLoeschen()</span>: Nimmt die Logbuch-ArrayList per Getter vom aufrufendem Objekt
     * und loescht alle Eintraege mit <i style="color:orange">clear()</i>.<br><br>
     * <span style="color:orange">brdCaster()</span>: Nimmt einen Scanner fuer Nachrichtenversandt, ein boolean
     * um zu ermitteln ob empfangen oder gesendet wird, sowie das feindlicher Raumschiff um Logbucheintraege & Broadcaster
     * setzen zu koennen. Gibt die gewuenschte Nachricht als String zurueck<br><br>
     * @param ladungen Ladung[]
     * @author Daniel Garkavy
     * @since 25-04-2021
     */

    void ladungsVerzeichnis(ArrayList<Ladung> ladungen) {
        for (int i = 0; i < ladungen.size(); i++) {
            System.out.printf("Ladung %d: %d %s (%s)\n", i + 1, ladungen.get(i).getAnzahl(), ladungen.get(i).getName(),
                              ladungen.get(i).getGrund());
        }
        System.out.println();
    }
    ArrayList<Ladung> ladungEntfernen(ArrayList<Ladung> ladungen) {
        ladungen.removeIf(la -> la.getAnzahl() == 0);
        return ladungen;
    }
    void logbuchLesen(ArrayList<String> a) {
        if (a.isEmpty()) {
            System.out.println("Logbuch ist momentan leer");
        }
        else {
            System.out.println("----------Logbuch Anfang----------");
            for (String s : a) {
                System.out.println(s);
            }
            System.out.println("----------Logbuch Ende----------");
        }
    }
    void logbuchLoeschen(ArrayList<String> a, Scanner in) {
        System.out.print("Sicher(y/n)?: ");
        boolean b = yesno(in);
        if (b) {
            a.clear();
            System.out.println("Eintraege geloescht!");
        }
        in.nextLine(); // <-- Löscht Scanner Buffer
    }
    String brdCaster(Scanner in, Raumschiff feind, boolean senden) {
        Date uhr = new Date();
        SimpleDateFormat uhrF = new SimpleDateFormat("HH:mm:ss");

        // Broadcaster sendet
        if (senden) {
            charArPrint("Nachricht setzen: ", 100);
            String s = in.nextLine();

            if (s == null || s.equals("")) {
                System.out.println("Leere Nachricht versendet.\n");
                return "_-~-__-~-_Leere Nachricht_-~-__-~-_";
            }

            logBuch.add(uhrF.format(uhr) + " Versandte Nachricht zu " + feind.getName().toUpperCase() + ": " + s);
            feind.logBuch.add(uhrF.format(uhr) + " Empfangene Nachricht von " + getName().toUpperCase() + ": " + s);
            System.out.println("Versandte Nachricht: "+s);

            return s;
        }

        // Broadcaster empfängt
        else {
            String empfangen = getBroadcastCom();
            feind.logBuch.add(uhrF.format(uhr) + " Empfangene Nachricht von " + getName().toUpperCase() + ": " + empfangen);
            logBuch.add(uhrF.format(uhr) + " Gesendet von " + feind.getName().toUpperCase() + ": " + empfangen);
            System.out.println("Empfangene Nachricht: "+empfangen);

            return empfangen;
        }
    }

    /**
     * <h1>Status Modifier</h1>
     * <span style="color:orange">treffer()</span>: Nimmt ein feindliches Raumschiff Objekt, fügt Logbucheintraege hinzu
     * und rechnet den Schaden aus/ermittelt ob die Huelle & energie schaden nimmt.<br><br>
     * <span style="color:orange">reparieren()</span>: Nimmt ein Scanner und gibt den errechneten Wert um den geheilt wird
     * zurueck (double <i style="color:yellow">heilung</i>). Nutzt java.security.SecureRandom um mit einer Entropietabelle
     * einen Wert zu errechnen, der schwer nachzuverfolgen ist (wegen hoher Entropie).<br><br>
     * @param feind Raumschiff
     * @author Daniel Garkavy
     * @since 25-04-2021
     */

    private void treffer(Raumschiff feind) {
        Date uhr = new Date();
        SimpleDateFormat uhrF = new SimpleDateFormat("HH:mm:ss");
        System.out.printf("[%s] wurde getroffen!\n", feind.getName());

        if (feind.getSchild() != 0) {
            feind.setSchild(feind.getSchild() - 0.5);
            feind.logBuch.add(uhrF.format(uhr)+" Schilde um 50% verringert.");
        }
        else if (feind.getSchild() <= 0 && feind.getHuelle() > 0) {
            feind.setHuelle(feind.getHuelle() - 0.5);
            feind.setEnergie(feind.getEnergie() - 0.5);
            if (feind.getHuelle() > 0) {
                feind.logBuch.add(uhrF.format(uhr) + " Huelle um 50% verringert.");
                feind.logBuch.add(uhrF.format(uhr) + " Energie um 50% verringert.");
                charArPrint("[" + feind.getName() + "]: \"Wir haben betraechtlichen Schaden erlitten!\"\n\n", 14);
            }
            else {
                feind.setHuelle(0);
                feind.setEnergie(0);
                feind.logBuch.add(uhrF.format(uhr) + " ..Wir haben verloren. :(");
                feind.logBuch.add(uhrF.format(uhr) + " Huelle ist nun 0%.");
                charArPrint("[" + feind.getName() + "]: \"Erhaltungssysteme sind komplett zerstört!! \"\n\n", 14);
                System.out.println("Sie haben Gewonnen!!");
                System.exit(0);
            }
        }
    }
    double reparieren(Scanner in) {
        SecureRandom rng = new SecureRandom();
        Date uhr = new Date();
        SimpleDateFormat uhrF = new SimpleDateFormat("HH:mm:ss");

        double zufall = rng.nextDouble();
        double heilung = 0;

        charArPrint("Wieviele Androiden einsetzen?: ", 23);
        int anzahl = in.nextInt();
        if (anzahl >= getAndroid()) {anzahl = getAndroid();}

        for (int i = anzahl; i > 0; i--) {
            heilung = (zufall * anzahl);
        }

        String formatiert = String.format(" Du hast dich um %.2f",heilung * 100);
        logBuch.add(uhrF.format(uhr) + formatiert + "% mit " + anzahl + " Androiden geheilt");
        formatiert = String.format("Geheilt um %.2f", heilung * 100);
        charArPrint(formatiert + "%", 20);
        waitPrint('\n',150, 1);
        charArPrint("Verbleibende Androiden-Anzahl: " + (getAndroid() - anzahl) + "\n", 15);

        setAndroid(getAndroid() - anzahl);
        in.nextLine();
        return heilung;
    }
}