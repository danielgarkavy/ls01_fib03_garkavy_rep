import java.io.*;
import java.util.ArrayList;

public class Captain {
    /*
       ###################
       #### Attribute ####
       ###################
    */
    private String name;
    private boolean mehrereSchiffe;
    private String geschlecht;
    private int captainSeit;

    /*
       ###################
       ### Konstruktor ###
       ###################
    */

    Captain() {}
    Captain(String name, boolean mehrereSchiffe, Inventar geschlecht, int captainSeit) {
        this.name = name;
        this.mehrereSchiffe = mehrereSchiffe;
        this.geschlecht = geschlecht.toString();
        this.captainSeit = captainSeit;
    }

    /*
      ###################
      # Getter & Setter #
      ###################
   */
    String getName() {
        return this.name;
    }
    void setName(String name) {
        this.name = name;
    }

    boolean getMehrereSchiffe() {
        return this.mehrereSchiffe;
    }
    void setMehrereSchiffe(boolean mehrereSchiffe) {
        this.mehrereSchiffe = mehrereSchiffe;
    }

    String getGeschlecht() {
        return this.geschlecht;
    }
    void setGeschlecht(Inventar geschlecht) {
        this.geschlecht = geschlecht.toString();
    }

    int getCaptainSeit() {
        return this.captainSeit;
    }
    void setCaptainSeit(int captainSeit) {
        this.captainSeit = captainSeit;
    }

     /*
       ###################
       ###  Funktionen ###
       ###################
    */

    /**
     * <h1>Speichern, Laden & Loeschen</h1>
     * Speichert Schiff, Captain und Ladung ab. Arraylisten kommen weit unten vor in den methoden
     * <span style="color:orange">speicherFile()</span> und <span style="color:orange">ladeFile()</span>,
     * da Elementlaengen variieren koennen.
     * Die Speicherdatei wird im Root-Ordner des Projektes erstellt und wird dort gelesen.
     * <br><br>
     * Der Savefile Reader hat unten die Chance, nicht-konstante Laengen zu lesen (wie, z.B, "Logbuch").
     * Es gibt ein Boolean zurueck, um das Spiel zu beenden, wenn erfolgreich gespeichert worden ist.
     * ___________________________________________________________________________________________
     * <br><br>
     * <span style="color:orange">speicherFile()</span>: Nimmt Raumschiffe als Argument und
     * speichert via lokalen <b>BufferedWriter</b> eine Speicherdatei. Bei dynamischen Eintraegen (<b>ArrayLists</b>
     * oder <b>File</b>) markiert es jeweils den Anfang, das naechste Objekt und das Ende des Arrays.
     * Gibt <b style="color:red">false</b> zurueck wenn erfolgreich und beendet das Spiel.
     * <br><br>
     * <span style="color:orange">ladeFile()</span>: Gibt die gespeicherten Schiffe zurueck, die aus der Speicherdatei
     * per <b>BufferedReader</b> ausgelesen werden. Kaptiaene und Ladungen sind daher auch im Schiffsobjekt vorhanden.
     * Alle konstanten Objekte werden zuerst generiert und danach werden variable Objekte (z.B. ArrayLists) erzeugt.
     * Erzeugt naechstes Objekt erst, wenn per <b>Regular-Expression/Reg-Ex</b> ein "Ende" gefunden worde.
     * <br><br>
     * <span style="color:orange">loescheSaveFile()</span>: Gibt <b style="color:red">false</b> zurueck und beendet
     * das Spiel, wenn per <b>File</b> Objekt die Datei <i>"SpeicherFile.txt"</i>
     * (die von <i>"speicherFile()"</i> im root Ordner des Projekts erzeugt worden ist) geloescht wird.
     * <br><br>
     * @author Daniel Garkavy
     * @since 24-04-2021
     * @param raumschiffe Schiffs-Array
     * @return raumschiff & boolean
     */

    public static boolean speicherFile(Raumschiff[] raumschiffe) {

         try {
             BufferedWriter speicherFile = new BufferedWriter(new FileWriter("SpeicherFile.txt"));

             // Schiffe
             for (Raumschiff raumschiff : raumschiffe) {
                 speicherFile.write(raumschiff.getName());
                 speicherFile.newLine();
                 speicherFile.write(raumschiff.getBroadcastCom());
                 speicherFile.newLine();
                 speicherFile.write(Double.toString(raumschiff.getEnergie()));
                 speicherFile.newLine();
                 speicherFile.write(Double.toString(raumschiff.getSchild()));
                 speicherFile.newLine();
                 speicherFile.write(Double.toString(raumschiff.getHuelle()));
                 speicherFile.newLine();
                 speicherFile.write(Integer.toString(raumschiff.getPhotonenTorpedo()));
                 speicherFile.newLine();
                 speicherFile.write(Integer.toString(raumschiff.getAndroid()));
                 speicherFile.newLine();
             }


             // Kapitaene
             for (Raumschiff raumschiff : raumschiffe) {
                 speicherFile.write(raumschiff.getKapitaen().getName());
                 speicherFile.newLine();
                 speicherFile.write(Boolean.toString(raumschiff.getKapitaen().getMehrereSchiffe()));
                 speicherFile.newLine();
                 speicherFile.write(raumschiff.getKapitaen().getGeschlecht());
                 speicherFile.newLine();
                 speicherFile.write(Integer.toString(raumschiff.getKapitaen().getCaptainSeit()));
                 speicherFile.newLine();
             }

             //  -----------------------------VARIABLE LAENGE-----------------------------

             // Ladungen
             for (int i = 0; i < raumschiffe.length; i++) {
                 if (i == 0) {
                     speicherFile.write("LADUNGSARRAY_ANFANG");
                     speicherFile.newLine();
                 }
                 for (Ladung parameter : raumschiffe[i].getLadung()) {
                     speicherFile.write(parameter.getName());
                     speicherFile.newLine();
                     speicherFile.write(Integer.toString(parameter.getAnzahl()));
                     speicherFile.newLine();
                     speicherFile.write(parameter.getGrund());
                     speicherFile.newLine();
                 }

                 if (i == raumschiffe.length - 1) {
                     speicherFile.write("LADUNGSARRAY_ENDE");
                 }
                 else {
                     speicherFile.write(raumschiffe[i].getName().toUpperCase().replace(" ", "_")
                             + "_LADUNG_ENDE");
                 }
                 speicherFile.newLine();
             }


             // Logbucheintraege
             for (int i = 0; i < raumschiffe.length; i++) {
                 if (i == 0) {
                     speicherFile.write("LOGBUCHARRAY_ANFANG");
                     speicherFile.newLine();
                 }
                 for (String a : raumschiffe[i].getLogBuch()) {
                     speicherFile.write(a);
                     speicherFile.newLine();
                 }

                 if (i == raumschiffe.length - 1) {
                     speicherFile.write("LOGBUCHARRAY_ENDE");
                 }
                 else {
                     speicherFile.write(raumschiffe[i].getName().toUpperCase().replace(" ", "_")
                             + "_LOGBUCH_ENDE");
                     speicherFile.newLine();
                 }
             }

             speicherFile.close();
         }

         catch (IOException ex) {
             System.out.println("Es konnte nicht gespeichert werden. Debug: " + ex);
             return true;
         }

         System.out.println("Erfolgreich gespeichert!");
         return false;
     }
    public static Raumschiff[] ladeFile() {

        Raumschiff[] arr = new Raumschiff[2];
        arr[0] = new Raumschiff();
        arr[1] = new Raumschiff();

        try {
            File check = new File("SpeicherFile.txt");
            if (!check.exists() || check.isDirectory()) {
                return null;
            }

            else {
                BufferedReader speicherFile = new BufferedReader(new FileReader("SpeicherFile.txt"));

                for (Raumschiff raumschiff : arr) {

                    // Schiffe
                    raumschiff.setName(speicherFile.readLine());
                    raumschiff.setBroadcastCom(speicherFile.readLine());
                    raumschiff.setEnergie(Double.parseDouble(speicherFile.readLine()));
                    raumschiff.setSchild(Double.parseDouble(speicherFile.readLine()));
                    raumschiff.setHuelle(Double.parseDouble(speicherFile.readLine()));
                    raumschiff.setPhotonenTorpedo(Integer.parseInt(speicherFile.readLine()));
                    raumschiff.setAndroid(Integer.parseInt(speicherFile.readLine()));
                }


                for (Raumschiff raumschiff : arr) {

                    // Kapitaene
                    Captain du = new Captain();
                    du.setName(speicherFile.readLine());
                    du.setMehrereSchiffe(Boolean.parseBoolean(speicherFile.readLine()));
                    du.setGeschlecht(Inventar.valueOf(speicherFile.readLine()));
                    du.setCaptainSeit(Integer.parseInt(speicherFile.readLine()));
                    raumschiff.setKapitaen(du);
                }


                // --------------------VARIABLE LAENGE--------------------


                // ++~   *************  =+ Ladungen +=  *************   ~++


                if (speicherFile.readLine().equals("LADUNGSARRAY_ANFANG")) {

                    for (Raumschiff raumschiff : arr) {
                        ArrayList<String> ladungenSchiff = new ArrayList<>();

                        while (ladungenSchiff.size() == 0 || !ladungenSchiff.get(ladungenSchiff.size() - 1)
                                .matches("[A-Z]+_*[A-Z]*_*ENDE$"))
                        {
                            ladungenSchiff.add(speicherFile.readLine());
                        }

                        raumschiff.setLadung(new ArrayList<>());

                        int counter = 0;
                        ArrayList<Ladung> la = raumschiff.getLadung();

                        for (int i = 0; i < (ladungenSchiff.size() - 1) / 3; i++) {

                            String name = ladungenSchiff.get(counter++);
                            int anzahl = Integer.parseInt(ladungenSchiff.get(counter++));
                            Inventar grund = Inventar.valueOf(ladungenSchiff.get(counter++));

                            la.add(new Ladung(name, anzahl, grund));
                        }

                        raumschiff.setLadung(la);
                    }
                }


                // ++~   *************  =+ Logbuch +=  *************   ~++


                if (speicherFile.readLine().equals("LOGBUCHARRAY_ANFANG")) {

                    for (Raumschiff raumschiff : arr) {
                        ArrayList<String> logbuchSchiff = new ArrayList<>();

                        while (logbuchSchiff.size() == 0 || !logbuchSchiff.get(logbuchSchiff.size() - 1)
                                .matches("[A-Z]+_[A-Z]*_*ENDE$"))
                        {

                            logbuchSchiff.add(speicherFile.readLine());
                        }
                        raumschiff.setLogBuch(logbuchSchiff);

                        if (raumschiff.getLogBuch().size() == 1 && raumschiff.getLogBuch().get(0).matches("[A-Z]+_[A-Z]*_*ENDE$"))
                        {
                            ArrayList<String> log = raumschiff.getLogBuch();
                            log.remove(0);
                            raumschiff.setLogBuch(log);
                        }
                    }
                }

                System.out.println("Alles erfolgreich geladen.");

                speicherFile.close();
            }
        }

        catch (IOException e) {
            System.out.println("Laden fehlgeschlagen. Debug: " + e);
            System.exit(410);
        }

        return arr;
    }
    public static boolean loescheSaveFile() {

        File speicherFile = new File("SpeicherFile.txt");

        if(speicherFile.delete()) {
            System.out.println("Erfolgreich geloescht!");
            return false;
        }

        else {
            System.out.println("SaveFile konnte nicht geloescht werden.");
            return true;
        }
    }
}