import javax.swing.*;
import java.awt.*;

public class EinTest extends JFrame{
    JPanel mainPanel;
    private JTextField eingabeFeld;
    private JButton rechner;
    private JLabel clicksLabel;
    private JLabel clickAdderLb;
    private JButton resetClicksButton;
    private int clicks = 0;


    public EinTest(String titel) {
        super(titel);
        this.mainPanel.setBackground(Color.PINK);

        this.setDefaultCloseOperation(EXIT_ON_CLOSE);
        this.setContentPane(mainPanel);
        this.pack();
        super.setBounds(40, 40, 400, 300);

        rechner.addActionListener(e -> {

            if (!eingabeFeld.getText().isEmpty()) {

                clickAdderLb.setText("Pro-Click Multiplier: " + eingabeFeld.getText());
                String etwas = eingabeFeld.getText();
                clicks += Integer.parseInt(etwas);
            }

            else {

                clickAdderLb.setText("Pro-Click Multiplier: Leer (einer Imkremente)");
                clicks++;
            }

            clicksLabel.setText("Anzahl: " + clicks);
        });


        resetClicksButton.addActionListener(e -> {

            clicks = 0;
            clicksLabel.setText("Anzahl: 0");
        });
    }
}
