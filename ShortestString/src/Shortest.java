import java.nio.file.Files;
import java.util.Arrays;
import java.lang.*;

public class Shortest {

    private static int shortest;
    private static Files In;

    public static int shortest(String s){
        String[] words = s.split("//s");

        int minimum = s.length();
        for (int i = 0; i < words.length; i++){
            if (words[i].length() < minimum){
                minimum = words[i].length();
            }
        }
        return shortest;

    }

    public static void main(String[] args) {
        String txt = "";
        System.out.println("Hello! I find the length of the shortest substring.");
        System.out.println("Please, enter a string:");
        txt = In.readString();

        System.out.println("The length is " + shortest(txt) + ".");
    }