import java.util.Arrays;

public class NextBiggerSwap {
    public static long n(){
        return 1234567890;
    }
    public static long nextBiggerNumber(long N) {
        if (N < 10) {return -1;}
        String s = "" + N;
        int[] ar = new int[s.length()];

        for (int i = 0; i < s.length(); i++) {
            char c = s.charAt(i);
            ar[i] = Character.getNumericValue(c);
        }

        int j;
        int pivot = 0;
        for (j = ar.length - 1; j > 0; j--) {
            if (ar[j] > ar[j - 1]) {pivot = j - 1; break;}
        }
        if (j == 0) {return -1;}
        // Swap procedures (bigger, but not next biggest n)
        int temp;
        for (int z = ar.length - 1; z > pivot; z--) {
            temp = ar[z];
            if (ar[pivot] < ar[z]) {
                ar[z] = ar[pivot];
                ar[pivot] = temp;
                pivot += 1;
                break;
            }
        }

        StringBuilder sb = new StringBuilder();
        Arrays.sort(ar, pivot, ar.length);
        for (int n : ar) {
            sb.append(n);
        }
        return Long.parseLong(sb.toString());
    }

    public static void main(String[] args){
        long n = n();
        long next = nextBiggerNumber(n);
        System.out.println("Expected: 1234567908");
        System.out.println("Biggest next is: " + next);
    }

}