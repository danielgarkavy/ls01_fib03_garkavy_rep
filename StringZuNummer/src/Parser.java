import java.util.TreeMap;

public class Parser {
    public static int parseInt(String numStr) {
        TreeMap<String, Integer> nums = new TreeMap<>();
        nums.put("zero", 0); nums.put("one", 1); nums.put("two", 2); nums.put("three", 3); nums.put("four", 4); nums.put("five", 5);
        nums.put("six", 6); nums.put("seven", 7); nums.put("eight", 8); nums.put("nine", 9); nums.put("ten", 10);
        nums.put("eleven", 11); nums.put("twelve", 12); nums.put("thirteen", 13); nums.put("fourteen", 14);nums.put("fifteen", 15); nums.put("sixteen", 16);
        nums.put("seventeen", 17); nums.put("eighteen", 18); nums.put("nineteen", 19);

        TreeMap<String, Integer> tens = new TreeMap<>();
        tens.put("twenty", 20); tens.put("thirty", 30); tens.put("forty", 40); tens.put("fifty", 50); tens.put("sixty", 60);
        tens.put("seventy", 70); tens.put("eighty", 80); tens.put("ninety", 90);

        TreeMap<String, Integer> hundreds = new TreeMap<>();
        hundreds.put("hundred", 100); hundreds.put("thousand", 1000); hundreds.put("million", 1000000);
        int tmp = 0, result = 0, prv = 0, mult = 0;

        String[] str = numStr.split(" ");
        for (int i = 0; i < str.length; i++) {
            if (nums.get(str[i]) != null) {
                tmp += nums.get(str[i]);
                if (mult < 1000 || i == str.length -1) {
                    result += tmp;
                    prv += result;
                }
                else {
                    prv = tmp;
                }
            } else if (hundreds.get(str[i]) != null) {
                if (result != 0 && result - prv == 0) {
                    result -= prv;
                }
                else if (mult < hundreds.get(str[i])) {
                    prv = result;
                    result = 0;
                }
                result += prv * hundreds.get(str[i]);
                tmp = 0;
                prv = 0;
                mult = hundreds.get(str[i]);
            } else if (tens.get(str[i]) != null) {
                tmp += tens.get(str[i]);
                result += tmp;
                prv = tmp;
            } else if (str[i].contains("-")) {
                String[] split = str[i].split("-");
                tmp += tens.get(split[0]);
                result += tmp;
                tmp = 0;

                tmp += nums.get(split[1]);
                result += tmp;
                prv += result;
            }

        }
        return result;
    }
    public static void main(String[] args) {
        System.out.println("Sollte sein: 200003");
        String test = "two hundred thousand three";
        int num = parseInt(test);
        System.out.println(num);
    }
}
