public class RailFenceCipher {
    int numRails;

    public RailFenceCipher(int numRails) {
        this.numRails = numRails;
    }

    public static void main(String[] args) {
        RailFenceCipher railFenceCipher = new RailFenceCipher(5);

        String data = "TestMitCases";
        System.out.println("Zu verschluesselnde String: " + data);

        String encrypted = railFenceCipher.encode(data);
        System.out.println("\nVerschluesselter Text: " + encrypted);

        String decrypted = railFenceCipher.getDecryptedData(encrypted);
        System.out.println("Entschluesselter Text: " + decrypted);
    }

    String encode(String s) {
        StringBuilder sb = new StringBuilder();
        int colLength = s.length();
        int count = 0;
        int key = numRails;
        boolean up = true;
        char[][] cipher = new char[key][colLength];

        for (int i = 0; i < colLength; i++) {
            if (count == 0 || count == key - 1) {
                up = !up;
            }
            cipher[count][i] = s.charAt(i);

            if (!up) {
                count++;
            }
            else {
                count--;
            }
        }
        for (int i = 0; i < key; i++) {
            for (int k = 0; k < colLength; k++) {
                if (cipher[i][k] != 0) {
                    sb.append(cipher[i][k]);
                }
            }
        }
        return sb.toString();
    }

    String getDecryptedData(String s) {
        char[] decrypted = new char[s.length()];
        int n = 0;
        for(int k = 0 ; k < numRails; k ++) {
            int index = k;
            boolean down = true;
            while(index < s.length() ) {
                System.out.println(k + " " + index+ " "+ n );
                decrypted[index] = s.charAt(n++);

                if(k == 0 || k == numRails - 1) {
                    // First or last Rail
                    index = index + 2 * (numRails - 1);
                }
                else if(down) {
                    index = index +  2 * (numRails - k - 1);
                    down = false;
                }
                else {
                    index = index + 2 * k;
                    down = true;
                }
            }
        }
        return new String(decrypted);
    }
}