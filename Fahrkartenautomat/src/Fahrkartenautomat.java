import java.util.Scanner;

class Fahrkartenautomat
{
    interface lambdaAusdruck {double run();}
    private static double kasse(double bestellung, Scanner tastatur){
        double wechselgeld = 27.2;
        if (bestellung > wechselgeld){
            System.out.printf("Nicht genug Geld in der Kasse (%.2f EURO): Das Admin-Menu wird ge�ffnet\n", wechselgeld);

            lambdaAusdruck admin = () -> {
                String[] s = new String[]{"[1]: Kasse leeren\n", "[2]: Wechselgeld hinzufuegen\n", "\n\nIhre Auswahl: "};
                for (String option : s) {
                    System.out.print(option);
                }
                int i;
                while (true) {
                    i = stringFaengerInt(tastatur);
                    switch (i) {
                        case 1 -> {
                            System.out.println("Kasse geleert!");
                            return 0.0;
                        }
                        case 2 -> {
                            System.out.print("Bitte einen neuen Kassenbetrag w�hlen: ");
                            return tastatur.nextDouble();
                        }
                        default -> System.out.print("Bitte erneut Option waehlen (nicht verfuegbare Option gewaehlt): ");
                    }
                }
            };
            double betrag = admin.run();
            System.out.printf("Kassenbetrag ist: %.2f EURO\n\n", betrag);
            warte(300);
            return betrag;
        }
        else {
            System.out.println("Kassenbetrag ist ausreichend. Fahre fort.");
            warte(500);
            return wechselgeld;
        }
    }
    private static void fahrkartenDaten(Scanner tastatur){
        double bestellung = fahrkartenbestellungErfassen(tastatur);
        double kasse = kasse(bestellung, tastatur);
        while (kasse < bestellung) {
            System.out.print("Immernoch unzureichender Kassenbetrag. (Bestellwert: " + bestellung + ").\n");
            kasse = kasse(bestellung, tastatur);
        }
        double bezahlung = fahrkartenBezahlen(tastatur, bestellung);
        fahrkartenAusgeben();
        rueckGeldAusgeben(bestellung, bezahlung);
    }
    private static int stringFaengerInt(Scanner tastatur){
        int eingabe = 0;
        boolean loop = false;
        while (!loop) {
            try {
                eingabe = tastatur.nextInt();
                loop = true;
            } catch (Exception e) {
                System.out.print("Bitte eine ganze Zahl eingeben: ");
                tastatur.next();
            }
        }
        return eingabe;
    }
    private static double stringFaengerDouble(Scanner tastatur){
        double eingabe = 0.0;
        boolean loop = false;
        while (!loop) {
            try {
                eingabe = tastatur.nextDouble();
                loop = true;
            } catch (Exception e) {
                System.out.print("Bitte einen Gueltigen Wert eingeben: ");
                tastatur.next();
            }
        }
        return eingabe;
    }
    private static void erneutKaufen(Scanner tastatur, char[] ladeChars){
        System.out.print("Erneut einkaufen? (Y/N) : ");
        String var = tastatur.next();
        if(var.equalsIgnoreCase("Y")){
            warte(500);
            lade(ladeChars);
            main(null);
        }
        else{
            System.out.print("Kaufvorgang abgebrochen\n");
        }
    }
    private static void lade(char[] ladeChars){
        for (int i = 0; i <= 11; i++){
            System.out.print("Fahrkartenautomat wird erneut initialisiert: " + ladeChars[i % 4] + "\r");
            warte(250);
        }
        System.out.print("\nAutomat ist wieder Betriebsbereit \n\n");
    }
    private static void warte(int millisekunde){
        try {
            Thread.sleep(millisekunde);
        } catch (InterruptedException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }
    private static void muenzeAusgeben(int zaehler, int[] muenzen){
        String[] muenzenPrint = {"   * * *   ", "  *     *  ", "Betrag hier :-|",
                "Waehrung hier bitte :)", "  *     *  ", "   * * *   "};
        int printAnzahl;
        System.out.println("Anzahl der auszugebenden Muenzen: " + zaehler);
        warte(400);
        for(int i = 0; i < zaehler; i += 3) {
            if(i + 3 <= zaehler) {
                printAnzahl = 3;
            }
            else {
                printAnzahl = zaehler % 3;
            }
            for(int reihe = 0; reihe < muenzenPrint.length; reihe++){
                for(int z = 0; z < printAnzahl; z++) {
                    if(reihe == 2 || reihe == 3) {//<-- schnellere Ausfuehrung durch ein Check
                        if (muenzen[i + z] <= 2) {
                            muenzenPrint[3] = " *  EURO * ";
                        } else {
                            muenzenPrint[3] = " *  CENT * ";
                        }
                        if (muenzen[i + z] < 10) {
                            muenzenPrint[2] = " *    " + muenzen[i + z] + "  * ";
                        } else {
                            muenzenPrint[2] = " *   " + muenzen[i + z] + "  * ";
                        }
                    }
                    System.out.print(muenzenPrint[reihe]);
                }
                System.out.println();
            }
        }
    }
    private static double fahrkartenbestellungErfassen(Scanner tastatur){
        double[] tarif = new double[]{2.9,3.3,3.6,1.9,8.6,9.0,9.6,23.5,24.3,24.9};
        int[] ticketAr = new int[]{0,0,0,0,0,0,0,0,0,0};
        String[] menu = new String[]{"[0]: Einzelfahrschein Berlin AB", "[1]: Einzelfahrschein Berlin BC", "[2]: Einzelfahrschein Berlin ABC",
                "[3]: Kurzstrecke", "[4]: Tageskarte Berlin AB", "[5]: Tageskarte Berlin BC", "[6]: Tageskarte Berlin ABC",
                "[7]: Kleingruppen-Tageskarte Berlin AB", "[8]: Kleingruppen-Tageskarte Berlin BC", "[9]: Kleingruppen-Tageskarte Berlin ABC"};

        double zuZahlenderBetrag = 0.0;
        int zaehler = 0;
        int auswahl;

        while (zaehler == 0) {
            System.out.println("Gewuenschte Fahrkarte auswaehlen: ");
            for (int i = 0; i < tarif.length; i++) {
                System.out.printf("%-39s | %.2f EURO\n", menu[i], tarif[i]);
            }
            System.out.print("\n\nIhre Auswahl eingeben: ");
            auswahl = stringFaengerInt(tastatur);
            while(auswahl > 9 || auswahl < 0) {
                System.out.print("Bitte gueltige Auswahl eingeben: ");
                auswahl = stringFaengerInt(tastatur);
            }
            System.out.print("Anzahl der Tickets: ");
            int tickets = stringFaengerInt(tastatur);
            while (tickets <= 0 || tickets > 10) {
                System.out.println("Es koennen nur 1 bis 10 Tickets erworben werden");
                System.out.print("Bitte erneut eingeben: ");
                tickets = stringFaengerInt(tastatur);
            }
            ticketAr[auswahl] += tickets;
            warte(400);

            System.out.print("Weitere Fahrkarten waehlen? (Y/N) : ");
            String yesno = tastatur.next();
            if(yesno.equalsIgnoreCase("Y")){
                warte(200);
            }
            else{
                zaehler = 1;
            }
        }
        for(int i = 0; i < ticketAr.length; i++){
            zuZahlenderBetrag += ticketAr[i] * tarif[i];
        }
        return zuZahlenderBetrag;
    }
    private static double fahrkartenBezahlen(Scanner tastatur, double zuzahlenMulti){
        double eingezahlterGesamtbetrag = 0.0;
        while (eingezahlterGesamtbetrag < zuzahlenMulti) {
            System.out.printf("Noch zu zahlen:  %.2f Euro\n", zuzahlenMulti - eingezahlterGesamtbetrag);
            System.out.print("Eingabe (mind. 5Ct, hoechstens 2 Euro): ");

            double eingeworfeneMuenze = stringFaengerDouble(tastatur);
            if(eingeworfeneMuenze == 2 || eingeworfeneMuenze == 1 ||
                    eingeworfeneMuenze == 0.5 || eingeworfeneMuenze == 0.2 || eingeworfeneMuenze == 0.1 ||
                    eingeworfeneMuenze == 0.05){
                    eingezahlterGesamtbetrag += eingeworfeneMuenze;
            }
            else{
                System.out.println("Muenze ungueltig. \nGueltige Muenzen: 2 EURO, 1 EURO, 50 CENT, 20 CENT, 10 CENT, 5 CENT");
                warte(600);
                System.out.printf("Rueckgabe von der eingeworfener Muenze: %.2f EURO\n", eingeworfeneMuenze);
                warte(800);
            }
        }
        return eingezahlterGesamtbetrag;
    }
    private static void fahrkartenAusgeben(){
        System.out.println("\nFahrschein wird ausgegeben");
        for (int i = 0; i < 8; i++) {
            System.out.print("=");
            warte(250);
        }
        System.out.println("\n\n");
    }
    private static void rueckGeldAusgeben(double zuZahlenMulti, double eingezahlterGesamtbetrag){
        int[] muenzen = new int[]{0,0,0,0,0}; //maximalanzahl an muenzausgaben = 5
        double rueckgabebetrag = (eingezahlterGesamtbetrag * 100) - (zuZahlenMulti * 100);
        if (rueckgabebetrag > 0.0) {
            System.out.printf("Der R�ckgabebetrag in H�he von %.2f EURO ", rueckgabebetrag / 100);
            System.out.println("wird in folgenden M�nzen ausgezahlt:");
            int zaehler = 0;
            while (rueckgabebetrag >= 200.0) // 2 EURO-M�nzen
            {
                rueckgabebetrag -= 200.0;
                muenzen[zaehler] = 2;
                zaehler++;
            }
            while (rueckgabebetrag >= 100.0) // 1 EURO-M�nzen
            {
                rueckgabebetrag -= 100.0;
                muenzen[zaehler] = 1;
                zaehler++;
            }
            while (rueckgabebetrag >= 50.0) // 50 CENT-M�nzen
            {
                rueckgabebetrag -= 50.0;
                muenzen[zaehler] = 50;
                zaehler++;
            }
            while (rueckgabebetrag >= 20.0) // 20 CENT-M�nzen
            {
                rueckgabebetrag -= 20.0;
                muenzen[zaehler] = 20;
                zaehler++;
            }
            while (rueckgabebetrag >= 10.0) // 10 CENT-M�nzen
            {
                rueckgabebetrag -= 10.0;
                muenzen[zaehler] = 10;
                zaehler++;
            }
            while (rueckgabebetrag >= 5.0)// 5 CENT-M�nzen
            {
                rueckgabebetrag -= 5.0;
                muenzen[zaehler] = 5;
                zaehler++;
            }
            muenzeAusgeben(zaehler, muenzen);
        }
    }
    private static void bestaetigung(){
        System.out.println("\nVergessen Sie nicht, den Fahrschein\n" +
                "vor Fahrtantritt entwerten zu lassen!\n" +
                "Wir wuenschen Ihnen eine gute Fahrt.");
    }
    public static void main(String[] args){
        char[] ladeChars = new char[]{'|', '/', '-', '\\'};
        Scanner tastatur = new Scanner(System.in);
        fahrkartenDaten(tastatur);
        bestaetigung();
        warte(1000);
        erneutKaufen(tastatur, ladeChars);
    }
}