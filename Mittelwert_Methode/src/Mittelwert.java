import java.util.Scanner;

public class Mittelwert {
    public static int wiederholen(Scanner scan){
        String var;
        System.out.print("Erneut Rechnen? (Y/N): ");
        var = scan.next();
        int zaehler;
        if(var.equalsIgnoreCase("Y")){
            schlaf(700);
            zaehler = 0;
        }
        else{
            zaehler = 1;
        }
        return zaehler;
    }
    public static void schlaf(int millisek){
        try{
            Thread.sleep(millisek);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
    public static double mittelwert(double x, double y){
        double m;
        m = (x + y) / 2.0;
        return m;
    }
    public static void main(String[] args) {
        int zaehler = 0;
        while(zaehler == 0){
            Scanner scan = new Scanner(System.in);
            System.out.print("Bitte X-Wert eingeben: ");
            double x = scan.nextDouble();
            System.out.print("Bitte Y-Wert eingeben: ");
            double y = scan.nextDouble();
            double ergebnis = mittelwert(x ,y);
            System.out.printf("Der Mittelwert von %.2f und %.2f ist %.2f\n", x, y, ergebnis);
            wiederholen(scan) = zaehler;
        }
    }
}
