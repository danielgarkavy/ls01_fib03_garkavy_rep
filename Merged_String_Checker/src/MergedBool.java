public class MergedBool {
    public static boolean isMerge(String s, String part1, String part2) {
        boolean a = false;
        boolean b = false;

        // Check for final iteration
        if (s.equals("") && (!part1.isEmpty() || !part2.isEmpty())) {return false;}
        if ((part1.length()  == 0 || part2.length()  == 0) && s.length()  == 0) {return true;}

        // Recursive method call: Chop 1 char off in each iteration
        if (part1.length() != 0 && s.charAt(0) == part1.charAt(0)) {
            a = isMerge(s.substring(1), part1.substring(1), part2);
        }
        if (part2.length() != 0 && s.charAt(0) == part2.charAt(0)) {
            b = isMerge(s.substring(1), part1, part2.substring(1));
        }

        // Debug output
        System.out.println("----------------");
        System.out.println("String: " + s);
        System.out.println("part1String: " + part1);
        System.out.println("part2String: " + part2 + "\n");
        System.out.println("a = " + a);
        System.out.println("b = " + b + "\n\n");

        return a || b;
    }
    public static void main(String[] args){
        String s = "**********************";
        boolean a = isMerge("Daniel", "Dan", "iel");
        System.out.print("\n\n" + s + "\n" + "This case is: " + a + "\n" + s + "\n");
    }
}