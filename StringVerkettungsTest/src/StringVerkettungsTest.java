import java.util.Scanner;

public class StringVerkettungsTest {
    public static void reiheMulti(Scanner input, int reihe){
        int multiplikator;
        System.out.print("Bitte Multiplikationsreihe eingeben: ");
        multiplikator = input.nextInt();
        for(int i = 1; i <= reihe; i++){
            System.out.print(i+" * "+multiplikator+" = "+(i*multiplikator)+"\n");
        }
    }
    public static void main(String[] args){
        Scanner input = new Scanner(System.in);
        reiheMulti(input, 10);
    }
}